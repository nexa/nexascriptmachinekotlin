# NexaScriptMachineKotlin

This is a Kotlin library to access the Nexa Script Virtual Machine.  

It can be used to implement a simulator or debugger.

## Documentation

https://nexa.gitlab.io/nexascriptmachinekotlin/

## Quick Start Installation

Look here https://gitlab.com/nexa/nexascriptmachinekotlin/-/packages for the latest package version.  Let's say its "9.9.9".
In the application level build.gradle.kts include the repository:
```
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven") }
}
```

Then include the library as a dependency:

```
dependencies {
implementation("Nexa","NexaScriptMachine","9.9.9")
}
```

or if you are using this library in your test code:

```
testImplementation("Nexa","NexaScriptMachine","9.9.9")
```



## Maintainer Notes

### Deployment

#### Setup

* In build.gradle.kts

* Get a project deploy token from gitlab web -> settings -> Repository -> Deploy tokens.

Name it "Deploy-Token".  Select write_registry in your scope.  **DO NOT CREATE AN ACCESS TOKEN (Settings->Access Tokens).  IT WONT WORK.**

* Place it in your local.properties file
> DeployTokenValue=xxxxxxxx


* Bump the version of the project in build.gradle.kts 

> version = "1.0.0"

* From the terminal run

> ./gradlew publish


* Verify by browsing to https://gitlab.com/api/v4/projects/46299034/packages

#### Setup for local JAR file creation

* File->Project Structure->Artifacts->+->JAR->From modules with dependencies
  (Create a manifest file: I'm unsure what to do here)
* Build->Build Artifacts