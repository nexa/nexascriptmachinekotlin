package org.nexa.scriptmachine

import com.ionspin.kotlin.bignum.integer.BigInteger
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.NexaScript
import kotlin.test.Test
import kotlin.test.*


class NexaScriptMachineTest
{
    @BeforeTest
    fun init()
    {
        Initialize()
    }

    @Test
    fun basicTest()
    {
        println("test begin")
    }

    @Test
    fun testTxParsing()
    {
        val txpHex =
            "000600bdc7b2943e225ec0347b9298c69a0934ab3c0cdf0d0daa8d7eedb7fcafac702564222102f1d201c14136c0e2ce731417e69fcf2839f218927bc2825e41a8ed6ad1802e2f4010ad22c6e26eb9c9bdff0c1fcd29b10e3e096d218d5b926560b1cd8d7de0b1bddbbf02bfa46a340a4917a79fcd5d89b5a960780a344264e0210dba66d90fdcfcfeffffffd03f440100000000007ccc8d93ace3d4b095dd447bc8ef57d8ea73b452cb8c83648a27c3e7b3ff5180642221024c536315861395d785e2a939be8d0a56f8c073f6fe381805d3562fcd5f45d14c40c6ff37c5a580c06e4a891abe41ff3c0cef9707c6986c1fd197005ec6508b61a193a81613e8044cf267b6985cb027eda2725980546ace6426289152c0838939dafefffffff1da9a3b0000000000b777053590d2ad796f4e23974ccd908edd24285abfd4971da1d560f5583a87b5642221035e959fac60e788e837b3e63ff8755dd814643954f3e14367403d8c1c80741474404be55d60cd6b25a149958c7f35ae504b8a8aac762b90fe1863b3d59eb6e70f2343cfc481142e138c121cdd618773c0f50e33171e0212aeebc1310ddbcb749308feffffff4c9c0d000000000000b360b07cc0e38bf8dbe892e6ad0b69e978c9b59d68b4a6acdc50a51c10a9f7fe642221028289ec94736df58d1ba51d53d52928bd6c02b502862f394e5e149024a947071e40ae8cea6673c8f4775edc7ffc5a083897ea45897326e4e9e13799a288f51aa0dd82a78269852ef9fda48219f60e6fedba1917d612b15d1017441f92e20354f2abfeffffff0017640700000000001f7928edd6de2605b223349fe138e05faa76047edbaf37b4863ef55e445151ff642221036b74361a868cde5e4869968793cc51a0510d899922c3cc6073664d7ac9ccbbd0406352c653dda78c67a25119ca0a392ced57622609f4cf573f3a130ef389adf065bf3f1cc5dbb23e52d1db9219dfedceb688e1a75d4237a4d21d862a3053078657feffffff2bd7dc010000000000db171df4477eeaad05319158cd6c8fa9a18f9be8985e04d9db0f6d3d005ecdff64222102ade93f3ad265d66dcfd42c7ae3c80898f902b231629fdbe9dcfcc35483bca7404099797dc2ec315876f3491dd266b1d27daace1fd398e20e89d1c1cbae7059b33c6a8245a251d86c95edd44571e3a179c729525a935b947aceb712d3cea5a9d567feffffff7badb903000000000201864c51000000000017005114c010e311349b28b1ab2aaa6102c94e6486081fa101780296490000000017005114ef8b65ed29fb260379e772db69d816b7956dcb24c1030000"
        val txcHex =
            "00010097d2862938b4b5a1cc3d043896e14daaa756b2deea1738852a1891937c86e5d064222103238b71f552908f0998d7d0cc3fdc07aeac5fb70108c59e949cfb67aac6a12ff040aa1682ced417d38aa730c73d3149e1cc7e7196dbf293226db3061ca41b60d473299fa95cb1cd7188c0c72da8353326df7e4f0d792ec150235d7521e0fc03478efeffffff78029649000000000201fd07010000000000170051146777dfa4983d59a7c431243d53881f51861fd07801a0f99449000000001700511452cfae8f7277ffea0e483712bd2150b94c802286c2030000"

        val txp = NexaTransaction.fromHex(ChainSelector.NEXAREGTEST, txpHex)
        val txc = NexaTransaction.fromHex(ChainSelector.NEXAREGTEST, txcHex)

        val serOuts = BCHserialized(SerializationType.NETWORK)
        serOuts.addlist(txp.outputs)
        val serOutHex = serOuts.toByteArray().toHex()
        //println("serOuts:" + serOutHex)

        val env = analyze2Tx(listOf(txc, txp))
        check(env != null)
        println(env?.utxo)
    }


    @Test
    fun buildTx()
    {
        val cs = ChainSelector.NEXATESTNET
        val h = Hash256()
        h[0] = 1  // Just set it to anything nonzero
        h[1] = 2
        h[31] = 50

        if (true)
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.DROP, OP.DROP)
            val constraint = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.toByteArray()), OP.push(constraint.toByteArray()))
            inputScript.add(satisfier.toByteArray())
            tx.add(NexaTxInput(cs, sp, inputScript) as iTxInput)  // this is the input we will debug
            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))  // not relevant to the debugger

            //println("tx to debug: " + tx.toHex())

            val sm = ScriptMachine(tx, 0, null, false)
            var scriptResult = sm.next()
            println(scriptResult.first)
            println("top of main stack: ${sm.mainStackAt(0)}")
            println("top of alt stack: ${sm.altStackAt(0)}")

            check(scriptResult.first == "constraint")
            check(scriptResult.second == "No error(0)")

            scriptResult = sm.next()
            check(scriptResult.first == "satisfier")
            check(scriptResult.second == "No error(0)")
            check(scriptResult.third == ScriptMachine.SpecialOperation.ALT_STACK_LOADED)

            scriptResult = sm.next()
            check(scriptResult.first == "template")
            check(scriptResult.second == "No error(0)")

            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
        }

        if (true) // try simple tx without a constraint args script
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.ADD, OP.C5, OP.EQUALVERIFY)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C2, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.toByteArray()))
            inputScript.add(satisfier.toByteArray())
            tx.add(NexaTxInput(cs, sp, inputScript))  // this is the input we will debug
            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))

            val sm = ScriptMachine(tx, 0, null, true)
            sm.next()
            check(sm.atEnd == true)
            check(sm.scriptErr == "No error(0)")
            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
        }

        if (true)  // try simple tx with all 3 scripts and a utxo
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.FROMALTSTACK, OP.ADD, OP.ADD, OP.C5, OP.EQUALVERIFY)
            val constraintPriv = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.toByteArray()), OP.push(constraintPriv.toByteArray()))
            inputScript.add(satisfier.toByteArray())
            tx.add(NexaTxInput(cs, sp, inputScript))  // this is the input we will debug
            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))

            //println("tx to debug: " + tx.toHex())

            val utxo = NexaTxOutput(cs, 1000, SatoshiScript.p2t(cs, template.scriptHash160(), constraintPriv.scriptHash160(), SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C1)))
            //println("utxo: " + utxo.toHex())
            val sm = ScriptMachine(tx, 0, utxo, true)
            sm.next()
            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
        }
    }

    @Test
    fun testExample()
    {
        val sm = ScriptMachine()
        val result = sm.eval(OP.push(1), OP.push(2), OP.ADD)
        println(sm.mainStackAt(0))
        sm.delete()
    }

    @Test
    fun testExample2()
    {
        // these are real Nexa mainnet transactions.  One spends the other
        val sm = ScriptMachine(
            "000200cb159a8e52e2c74ad429c2de88f17ef319f86f9590a8fa1f098db38f313a2b6264222103b64731809e5f319da2e24bde8fd8e3932a8eff95e52f8c27fb404e28f1ccbd6f40f2243c823ecd9b47dd9ff898a01549b4a87bab9641596d97bb550b7cde6eaf68f15ebe4939579e28edc7a5065be2229be273a8e4e5f79ceb08cb855c1339c733feffffff8f32e732000000000020f555e60ef26d64afeac8d11b21ce257168e4d3fbb7b50c6e7a74d024db0f0b642221033e3785bf307e019a934953df869812d92529d731ad0dd680ce7f827d707725d4409167b36a8f3a5d1af679bc02133a800065159381884fcc618ef555cfd195e1e5a7f68bb68521c8a7e1333ebd5cdc046fd6e9f3a16fe1e3c63aa6c2e5f9f44865feffffff27800100000000000101d2aee83200000000170051149b95d76faca0caa4b0c70648a3f6aa279d887254c84c0400",
            "000400c87f900101d8ea4b80c015166e86e6edee7a46e7040cab812618066c41cff73b6422210388d574eef524c90d17d82c3616500265463d31fbc030699da33dd7f2dc280279400e520d58d96546333d228fc0e2acfce1844c3c2c87ce8e0c010492a8157bfcdbfe064d8571aa6a2e6b055b87ae96bb664add7e41508c17cf28aac08f20761123feffffff1ad0ff02000000000019d53ff57314cb2ee8d180050d4e7e055b6f6b8aa4a65871c9486960b185944f64222103efd9aa05dafd563fda6ecdb8dba647b95233f6f7d23abec07525a8c1881491a740c69c1b3d07bfe691cd1cafaae0424649e6313c4c3557765b05ddb0a783d117a2ead5ef7a9b7510bc65fb66387ae7a67754551cbd0a9b46f9b090aaf928939bc7feffffffa9db37000000000000c31e4fee1bfaa89f74a912ab3e2fb8cc3c55b8bf806fda59a2ad3c4bce9f735064222103b1f8d7d1f23e09edfa2fb8ab6f85f9511b7b946863fcddb507243d05052b8b05408389dbf9e303c47e5f961df1211afe934ec5393e3ac487de0cfc496eeedf5e372b3ed943977e462c528648c2a963fc0fac2aa327a0ef0c3e278e16592bae2a0dfeffffff850a0d000000000000fb3feaed598c907f6e905eb8e77326f3177a8ff7734608e0e5d6ace23bcc27a1642221026c2a2a3283ef5f308770c4e17f491428545c1e9cce714b2e1379ba28f093200940630f0ba44c4f51cde446ce6531da20a339aa3f7745f9acf936bbc51f57a36ccd7b3530c1ff5e6896d3d67742b0af22694bc6ebc0010388ecd666a4b2760a4da8feffffff0008af2f00000000020128890c000000000017005114f7ebf308b8396180f0cc63fc0b8ad686f24a1e8d018f32e7320000000017005114e919c8b37f76c08bdc1ec0ef38384e792c71aa6489470400"
                              )
        var ok = true
        println("Template is: " + sm.template!!.toAsm(" "))
        println("Constraint Arg is: " + sm.constraint!!.toAsm(" "))
        println("Satisfier Arg is: " + sm.satisfier!!.toAsm(" "))
        var instructionCount = 0
        sm.next(false)   // true means run, false means prepare to step
        do {
            ok = sm.step()
            if (ok) instructionCount++
        } while(ok)
        println("Result is: ${sm.scriptErr} in $instructionCount instructions")
        sm.delete()
    }

    @Test
    fun testScriptOverride()
    {
        val override = NexaScript(OP.FROMALTSTACK, OP.EQUALVERIFY)  // will fail
        // these are real Nexa mainnet transactions.  One spends the other
        val sm = ScriptMachine(
            "000200cb159a8e52e2c74ad429c2de88f17ef319f86f9590a8fa1f098db38f313a2b6264222103b64731809e5f319da2e24bde8fd8e3932a8eff95e52f8c27fb404e28f1ccbd6f40f2243c823ecd9b47dd9ff898a01549b4a87bab9641596d97bb550b7cde6eaf68f15ebe4939579e28edc7a5065be2229be273a8e4e5f79ceb08cb855c1339c733feffffff8f32e732000000000020f555e60ef26d64afeac8d11b21ce257168e4d3fbb7b50c6e7a74d024db0f0b642221033e3785bf307e019a934953df869812d92529d731ad0dd680ce7f827d707725d4409167b36a8f3a5d1af679bc02133a800065159381884fcc618ef555cfd195e1e5a7f68bb68521c8a7e1333ebd5cdc046fd6e9f3a16fe1e3c63aa6c2e5f9f44865feffffff27800100000000000101d2aee83200000000170051149b95d76faca0caa4b0c70648a3f6aa279d887254c84c0400",
            "000400c87f900101d8ea4b80c015166e86e6edee7a46e7040cab812618066c41cff73b6422210388d574eef524c90d17d82c3616500265463d31fbc030699da33dd7f2dc280279400e520d58d96546333d228fc0e2acfce1844c3c2c87ce8e0c010492a8157bfcdbfe064d8571aa6a2e6b055b87ae96bb664add7e41508c17cf28aac08f20761123feffffff1ad0ff02000000000019d53ff57314cb2ee8d180050d4e7e055b6f6b8aa4a65871c9486960b185944f64222103efd9aa05dafd563fda6ecdb8dba647b95233f6f7d23abec07525a8c1881491a740c69c1b3d07bfe691cd1cafaae0424649e6313c4c3557765b05ddb0a783d117a2ead5ef7a9b7510bc65fb66387ae7a67754551cbd0a9b46f9b090aaf928939bc7feffffffa9db37000000000000c31e4fee1bfaa89f74a912ab3e2fb8cc3c55b8bf806fda59a2ad3c4bce9f735064222103b1f8d7d1f23e09edfa2fb8ab6f85f9511b7b946863fcddb507243d05052b8b05408389dbf9e303c47e5f961df1211afe934ec5393e3ac487de0cfc496eeedf5e372b3ed943977e462c528648c2a963fc0fac2aa327a0ef0c3e278e16592bae2a0dfeffffff850a0d000000000000fb3feaed598c907f6e905eb8e77326f3177a8ff7734608e0e5d6ace23bcc27a1642221026c2a2a3283ef5f308770c4e17f491428545c1e9cce714b2e1379ba28f093200940630f0ba44c4f51cde446ce6531da20a339aa3f7745f9acf936bbc51f57a36ccd7b3530c1ff5e6896d3d67742b0af22694bc6ebc0010388ecd666a4b2760a4da8feffffff0008af2f00000000020128890c000000000017005114f7ebf308b8396180f0cc63fc0b8ad686f24a1e8d018f32e7320000000017005114e919c8b37f76c08bdc1ec0ef38384e792c71aa6489470400",
            override)

        var ok = true
        println("Template is: " + sm.template!!.toAsm(" "))
        println("Constraint Arg is: " + sm.constraint!!.toAsm(" "))
        println("Satisfier Arg is: " + sm.satisfier!!.toAsm(" "))
        var instructionCount = 0
        sm.next(false)   // true means run, false means prepare to step
        println("${sm.getState()}")
        do {
            ok = sm.step()
            if (ok) instructionCount++
        } while(ok)
        println("Result is: ${sm.scriptErr} in $instructionCount instructions")
        check(sm.scriptErr!!.contains("failed"))
        sm.delete()
    }

    @Test
    fun testHDderivation()
    {
        val secret = ByteArray(32, { 1 })
        val newSecret = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, true, 0)
        //val newSecret = Native.Hd44DeriveChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, index.toInt())
        //val newSecret = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, index).first
        //println("HD key: " + newSecret.toHex())
        check(newSecret.first.toHex() == "9c219cb84e3199b076aaa0f954418407b1e8d54f79103612fbaf04598bc8be55")

        val newSecret2 = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, 0)
        //println("HD key: " + newSecret2.toHex())
        check(newSecret2.first.toHex() == "bc732a2fafc593296ee8447bc3694be5e9d26827a6cd553248976dc30e566829")

        val newSecret3 = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, 1)
        //println("HD key: " + newSecret3.toHex())
        check(newSecret3.first.toHex() == "4d0f9fcd6675e49e2e884f62ca86152877ae8addbdcef985d81afd507230de9b")
    }

    @Test
    fun testBin2BigNumNegative() // Bignum converting differently from documentation here https://spec.nexa.org/script/bignum/
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()
        val num = -123L
        sm2.eval(SatoshiScript(cs, ss, OP.push(num.leSignMag(0)), OP.BIN2BIGNUM), false)
        sm2.cont()
        val bignum = sm2.mainStackAt(0).split(" ")[3]
        check(bignum == num.toString())
    }

    @Test
    fun testBMD()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()

        sm2.bmd = "FFFF"
        println(sm2.bmd)
        check(sm2.bmd == "ffff")
    }

    @Test
    fun testStatus()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm = ScriptMachine()
        var result = sm.eval(SatoshiScript(cs, ss, OP.TMPL_SCRIPT))  // TMPL are placeholders not real opcodes
        println(sm.status)
        check(sm.status.contains("Opcode missing"))
        sm.clearStatus()
        check(sm.status.contains("Initialized"))
    }

    @Test
    fun testRegisters()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm = ScriptMachine()

        sm.setRegister(0, 1234)
        var ret = sm.getRegister(0)
        println(ret)
        check(ret.split(" ")[3] == "1234")
        sm.setRegister(0, -1234)
        ret = sm.getRegister(0)
        println(ret)
        check(ret.split(" ")[3] == "-1234")

        var v = BigInteger.fromInt(2345)
        sm.setRegister(1, v)
        ret = sm.getRegister(1)
        println(ret)
        check(ret.split(" ")[0] == "BIGNUM")
        check(BigInteger.parseString(ret.split(" ")[2].dropLast(1),16).toString(10) == "2345")
        check(ret.split(" ")[3] == "2345")

        v = BigInteger.fromInt(-2345)
        sm.setRegister(1, v)
        ret = sm.getRegister(1)
        println(ret)
        check(ret.split(" ")[0] == "BIGNUM")
        check(ret.split(" ")[3] == "-2345")

        sm.setRegister(2, byteArrayOf(4,5,6,7,8,9,0,1,2))
        ret = sm.getRegister(2)
        println(ret)
        check(ret.split(" ")[0] == "BYTES")
        check(ret.split(" ")[2] == "040506070809000102h")

        // recall the ending 80 means negative
        sm.setRegisterToBigNum(3, "50607080" )
        ret = sm.getRegister(3)
        println(ret)
        check(ret.split(" ")[0] == "BIGNUM")
        check(ret.split(" ")[2] == "-506070h")

        // recall the ending 00 means positive
        sm.setRegisterToBigNum(3, "5060708000" )
        ret = sm.getRegister(3)
        println(ret)
        check(ret.split(" ")[0] == "BIGNUM")
        check(ret.split(" ")[2] == "50607080h")

        // Make sure reg 0 was not changed
        ret = sm.getRegister(0)
        check(ret.split(" ")[3] == "-1234")
    }

    @Test
    fun scriptResources()
    {
        val cs = ChainSelector.NEXAREGTEST
        val h = Hash256()
        h[0] = 1  // Just set it to anything nonzero

        if (true) // try simple tx without a constraint args script
        {
            val tx = NexaTransaction(cs)
            val sp = Spendable(cs, NexaTxOutpoint(h), 1000)
            val template = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.ADD, OP.C5, OP.push(byteArrayOf(1,2,3,4)), OP.DROP, OP.EQUALVERIFY)
            val satisfier = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.C2, OP.C3)

            val inputScript = SatoshiScript(cs, SatoshiScript.Type.PUSH_ONLY, OP.push(template.toByteArray()))
            inputScript.add(satisfier.toByteArray())
            tx.add(NexaTxInput(cs, sp, inputScript))  // this is the input we will debug
            tx.add(NexaTxOutput(cs, 900, SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.PUSHTRUE)))

            var sm = ScriptMachine(tx, 0, null, true)
            sm.next()
            check(sm.atEnd == true)
            check(sm.scriptErr == "No error(0)")
            check(sm.mainStackAt(0) == "")  // If everything executed properly, main stack should be clear
            val smres = sm.getResources()
            check(smres.sigsChecked == 0L)
            // Recall that "instructions" does not include pushing low value constants, e.g. OP.C0 to OP.C16
            check(smres.instructionsExecuted == 3L)
            check(smres.maxStackBytes == 6L)
            check(smres.maxStackItems == 3L)

            // Check that most resource use persists
            sm.eval(SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.C1, OP.C5, OP.ADD, OP.DUP, OP.ADD, OP.DROP))
            sm.get(smres)
            // Instructions executed gets reset every eval
            check(smres.instructionsExecuted == 4L)
            // But the rest of the resources persist
            check(smres.maxStackBytes == 6L)

            // Try after resetting resource use
            sm.resetResourceUse()
            sm.eval(SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.C1, OP.C5, OP.ADD, OP.DUP, OP.ADD, OP.DROP))
            sm.get(smres)
            check(smres.instructionsExecuted == 4L)
            check(smres.maxStackBytes == 2L)

            // Try with such a small limit that the script will fail
            val sm2 = ScriptMachine(tx, 0, null, true)
            sm2.setLimits(maxStackUse = 3)
            sm2.next()
            check(sm2.scriptErr!!.contains("Stack total length limit exceeded"))
        }

    }


    @Test
    fun testClone()
    {
        // these are real Nexa mainnet transactions.  One spends the other
        val sm = ScriptMachine(
            "000200cb159a8e52e2c74ad429c2de88f17ef319f86f9590a8fa1f098db38f313a2b6264222103b64731809e5f319da2e24bde8fd8e3932a8eff95e52f8c27fb404e28f1ccbd6f40f2243c823ecd9b47dd9ff898a01549b4a87bab9641596d97bb550b7cde6eaf68f15ebe4939579e28edc7a5065be2229be273a8e4e5f79ceb08cb855c1339c733feffffff8f32e732000000000020f555e60ef26d64afeac8d11b21ce257168e4d3fbb7b50c6e7a74d024db0f0b642221033e3785bf307e019a934953df869812d92529d731ad0dd680ce7f827d707725d4409167b36a8f3a5d1af679bc02133a800065159381884fcc618ef555cfd195e1e5a7f68bb68521c8a7e1333ebd5cdc046fd6e9f3a16fe1e3c63aa6c2e5f9f44865feffffff27800100000000000101d2aee83200000000170051149b95d76faca0caa4b0c70648a3f6aa279d887254c84c0400",
            "000400c87f900101d8ea4b80c015166e86e6edee7a46e7040cab812618066c41cff73b6422210388d574eef524c90d17d82c3616500265463d31fbc030699da33dd7f2dc280279400e520d58d96546333d228fc0e2acfce1844c3c2c87ce8e0c010492a8157bfcdbfe064d8571aa6a2e6b055b87ae96bb664add7e41508c17cf28aac08f20761123feffffff1ad0ff02000000000019d53ff57314cb2ee8d180050d4e7e055b6f6b8aa4a65871c9486960b185944f64222103efd9aa05dafd563fda6ecdb8dba647b95233f6f7d23abec07525a8c1881491a740c69c1b3d07bfe691cd1cafaae0424649e6313c4c3557765b05ddb0a783d117a2ead5ef7a9b7510bc65fb66387ae7a67754551cbd0a9b46f9b090aaf928939bc7feffffffa9db37000000000000c31e4fee1bfaa89f74a912ab3e2fb8cc3c55b8bf806fda59a2ad3c4bce9f735064222103b1f8d7d1f23e09edfa2fb8ab6f85f9511b7b946863fcddb507243d05052b8b05408389dbf9e303c47e5f961df1211afe934ec5393e3ac487de0cfc496eeedf5e372b3ed943977e462c528648c2a963fc0fac2aa327a0ef0c3e278e16592bae2a0dfeffffff850a0d000000000000fb3feaed598c907f6e905eb8e77326f3177a8ff7734608e0e5d6ace23bcc27a1642221026c2a2a3283ef5f308770c4e17f491428545c1e9cce714b2e1379ba28f093200940630f0ba44c4f51cde446ce6531da20a339aa3f7745f9acf936bbc51f57a36ccd7b3530c1ff5e6896d3d67742b0af22694bc6ebc0010388ecd666a4b2760a4da8feffffff0008af2f00000000020128890c000000000017005114f7ebf308b8396180f0cc63fc0b8ad686f24a1e8d018f32e7320000000017005114e919c8b37f76c08bdc1ec0ef38384e792c71aa6489470400"
                              )
        var ok = true
        println("Template is: " + sm.template!!.toAsm(" "))
        println("Constraint Arg is: " + sm.constraint!!.toAsm(" "))
        println("Satisfier Arg is: " + sm.satisfier!!.toAsm(" "))
        var instructionCount = 0
        sm.next(false)   // true means run, false means prepare to step
        val clones = mutableListOf<ScriptMachine>()
        do {
            ok = sm.step()
            if (ok)
            {
                instructionCount++
                clones.add(sm.clone())
            }
        } while(ok)
        println("Result is: ${sm.scriptErr} in $instructionCount instructions")

        for (c in clones)
        {
            c.cont()
            println("Clone Result is: ${c.scriptErr} state: ${c.getState()}")
            check(c.pos == sm.pos)
            c.delete()
        }

    }

    @Test
    fun testDebugger()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val txpHex = "000600bdc7b2943e225ec0347b9298c69a0934ab3c0cdf0d0daa8d7eedb7fcafac702564222102f1d201c14136c0e2ce731417e69fcf2839f218927bc2825e41a8ed6ad1802e2f4010ad22c6e26eb9c9bdff0c1fcd29b10e3e096d218d5b926560b1cd8d7de0b1bddbbf02bfa46a340a4917a79fcd5d89b5a960780a344264e0210dba66d90fdcfcfeffffffd03f440100000000007ccc8d93ace3d4b095dd447bc8ef57d8ea73b452cb8c83648a27c3e7b3ff5180642221024c536315861395d785e2a939be8d0a56f8c073f6fe381805d3562fcd5f45d14c40c6ff37c5a580c06e4a891abe41ff3c0cef9707c6986c1fd197005ec6508b61a193a81613e8044cf267b6985cb027eda2725980546ace6426289152c0838939dafefffffff1da9a3b0000000000b777053590d2ad796f4e23974ccd908edd24285abfd4971da1d560f5583a87b5642221035e959fac60e788e837b3e63ff8755dd814643954f3e14367403d8c1c80741474404be55d60cd6b25a149958c7f35ae504b8a8aac762b90fe1863b3d59eb6e70f2343cfc481142e138c121cdd618773c0f50e33171e0212aeebc1310ddbcb749308feffffff4c9c0d000000000000b360b07cc0e38bf8dbe892e6ad0b69e978c9b59d68b4a6acdc50a51c10a9f7fe642221028289ec94736df58d1ba51d53d52928bd6c02b502862f394e5e149024a947071e40ae8cea6673c8f4775edc7ffc5a083897ea45897326e4e9e13799a288f51aa0dd82a78269852ef9fda48219f60e6fedba1917d612b15d1017441f92e20354f2abfeffffff0017640700000000001f7928edd6de2605b223349fe138e05faa76047edbaf37b4863ef55e445151ff642221036b74361a868cde5e4869968793cc51a0510d899922c3cc6073664d7ac9ccbbd0406352c653dda78c67a25119ca0a392ced57622609f4cf573f3a130ef389adf065bf3f1cc5dbb23e52d1db9219dfedceb688e1a75d4237a4d21d862a3053078657feffffff2bd7dc010000000000db171df4477eeaad05319158cd6c8fa9a18f9be8985e04d9db0f6d3d005ecdff64222102ade93f3ad265d66dcfd42c7ae3c80898f902b231629fdbe9dcfcc35483bca7404099797dc2ec315876f3491dd266b1d27daace1fd398e20e89d1c1cbae7059b33c6a8245a251d86c95edd44571e3a179c729525a935b947aceb712d3cea5a9d567feffffff7badb903000000000201864c51000000000017005114c010e311349b28b1ab2aaa6102c94e6486081fa101780296490000000017005114ef8b65ed29fb260379e772db69d816b7956dcb24c1030000"
        val txcHex = "00010097d2862938b4b5a1cc3d043896e14daaa756b2deea1738852a1891937c86e5d064222103238b71f552908f0998d7d0cc3fdc07aeac5fb70108c59e949cfb67aac6a12ff040aa1682ced417d38aa730c73d3149e1cc7e7196dbf293226db3061ca41b60d473299fa95cb1cd7188c0c72da8353326df7e4f0d792ec150235d7521e0fc03478efeffffff78029649000000000201fd07010000000000170051146777dfa4983d59a7c431243d53881f51861fd07801a0f99449000000001700511452cfae8f7277ffea0e483712bd2150b94c802286c2030000"

        val sm = ScriptMachine(txpHex, txcHex)
        var result = sm.eval(SatoshiScript(cs, ss, OP.PUSHTRUE))
        check(result)
        var stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BYTES 1 01h 1")
        result = sm.eval(SatoshiScript(cs, ss, OP.PUSHFALSE))
        check(result)
        stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BYTES 0 false 0")
        // Create a bigger-than 8 bytes bignum and validate proper printout
        result = sm.eval(SatoshiScript(cs, ss, OP.push(byteArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)), OP.SETBMD, OP.push(byteArrayOf(1,2,3,4,5,6,7,8,9,0xa,0xb,0xc)), OP.BIN2BIGNUM))
        check(result)
        stk = sm.mainStackAt(0)
        println(stk)
        check(stk == "BIGNUM 12 c0b0a090807060504030201h 3727165692135864801209549313")
        // Create a bigger-than 8 bytes bignum and validate proper printout
        result = sm.eval(SatoshiScript(cs, ss, OP.push(byteArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)), OP.SETBMD, OP.push(byteArrayOf(1,2,3,4,5,6,7,8,9,0xa,0xb,0xc,-128)), OP.BIN2BIGNUM))
        check(result)
        stk = sm.mainStackAt(0)
        check(stk == "BIGNUM 12 -c0b0a090807060504030201h -3727165692135864801209549313")
        println(stk)
        // Nothing there
        stk = sm.mainStackAt(10)
        println(stk)
        check(stk == "")

        val sm2 = ScriptMachine()
        result = sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1, OP.PICK, OP.C3, OP.PLACE))
        check(result)
        var i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        stk = sm2.mainStackAt(3)
        check(stk == "BYTES 1 05h 5")

        // Place the result indexing from the bottom of the stack (with a negative number)
        sm2.initialize()
        result = sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1, OP.PICK, OP.CNEG1, OP.PLACE))
        check(result)
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        stk = sm2.mainStackAt(3)
        check(stk == "BYTES 1 05h 5")

        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C5, OP.C2, OP.C1), false)
        sm2.step()
        sm2.modify(3, OP.DROP.v)  // modify the last instruction to be a DROP, so C5 will be the top
        sm2.cont()
        stk = sm2.mainStackAt(0)
        check(stk == "BYTES 1 05h 5")

        // Breakpoints the hard way
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.step()
        sm2.modify(2, byteArrayOf(-1))  // modify the last instruction to be a DROP, so C5 will be the top
        sm2.cont()
        stk = sm2.mainStackAt(0)
        check(stk == "BYTES 1 01h 1")
        sm2.modify(2, OP.C2)  // put it back
        sm2.cont(-1) // continue executing from the replaced instruction
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")

        // Breakpoints the easy way
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(1)
        sm2.setBreakpoint(3)
        sm2.cont()
        sm2.cont()
        sm2.cont()
        i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        check(i==6)
    }

    @Test
    fun testBreakpointStep() // Currently breakpoints cause step() to not run the line
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C0, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(1)
        sm2.step()
        sm2.step()
        sm2.step()
        sm2.step()
        sm2.step()
        var stk: String

        var i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        check(i==6)
    }

    @Test
    fun testBreakpointCont() // A breakpoint on the last line allows cont() to repeatedly rerun the last line
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(3)
        sm2.cont()
        sm2.cont()
        sm2.cont()
        sm2.cont()
        sm2.cont()
        var stk: String

        var i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        check(i==5)
    }
    @Test
    fun testBreakpointToggling() // Continue to a breakpoint, remove the breakpoint, and continue
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(2)
        sm2.cont()
        sm2.clearBreakpoint(2)
        sm2.cont()

        var stk: String

        var i = 0
        do
        {
            stk = sm2.mainStackAt(i)
            println(stk)
            i++
        } while (stk!="")
        check(i==5)
    }
    @Test
    fun testBreakpointPos()
    {
        val cs = ChainSelector.NEXAREGTEST
        val ss = SatoshiScript.Type.SATOSCRIPT
        val sm2 = ScriptMachine()
        sm2.initialize()
        sm2.eval(SatoshiScript(cs, ss, OP.C1, OP.C2, OP.C3, OP.C4), false)
        sm2.setBreakpoint(2)
        sm2.cont()
        check(sm2.pos==2)
    }
}