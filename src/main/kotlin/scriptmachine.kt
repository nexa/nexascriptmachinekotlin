package org.nexa.scriptmachine

import java.util.logging.Logger
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.ScriptMachine.Companion.getBMD
import org.nexa.libnexakotlin.ScriptMachine.Companion.getError
import org.nexa.libnexakotlin.ScriptMachine.Companion.getPos
import org.nexa.libnexakotlin.ScriptMachine.Companion.getStackItemText
import org.nexa.libnexakotlin.ScriptMachine.Companion.setPos
import com.ionspin.kotlin.bignum.integer.BigInteger
import org.nexa.libnexakotlin.ScriptMachine.Companion.create
import org.nexa.libnexakotlin.ScriptMachine.Companion.setResourceLimits

private val LogIt = Logger.getLogger("bu.scriptmachine")

/** All exceptions are derived from this class */
open class ScriptMachineException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Abnormal) :
    LibNexaException(msg, shortMsg, severity)

/** The actual script that underlies well known script #1 (pay to public key template) */
val p2pkt = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, OP.FROMALTSTACK, OP.CHECKSIGVERIFY)

/** This class captures the 3 scripts that interact to form spending constraints and their solution.
 *
 * */
data class SpecifiedScriptTemplate(val satisfier: SatoshiScript, val constraint: SatoshiScript, val template: SatoshiScript)


/** A succinct description of the script machine's current state */
data class MachineState(
    /** What role this script takes: satisfier, constraint, template, or null (meaning unknown or no script loaded) */
    var scriptType:String?,
    /** Current execution position within the script (in bytes).  null means the script has not been set up yet. */
    var pos: Int?,
    /** Error status */
    var status: String,
    /** Bignum modulo divisor */
    var bmd: String,
    /** Main stack contents.  The end of this list is the top of the stack */
    var mainstack:List<String>,
    /** Alt stack contents.  The end of this list is the top of the stack */
    var altstack:List<String>)


/** The information required to execute a script that uses introspection.
 * This consists of the utxo being spent, the input spending it, and the full transaction (for introspection)
 * If you want to simulate/debug a full transaction validation, this class allows you to organize the required data.
 * @see [analyze2Tx] to automatically extract the relevant data from parent and child transactions.
 */
class ScriptMachineEnvironment(
    val utxo: NexaTxOutput,
    val utxoIdx: Int,
    val spender: NexaTxInput,
    val tx: NexaTransaction)

enum class ScriptMachineStack
{
    MAINSTACK,
    ALTSTACK
}

/** Helper function that takes an array of bytes and returns them as a script IF those bytes "looks like"
 * (is full of push-only instructions and data) a constraint script
 */
fun probablyConstraintScript(bytes: ByteArray, cs:ChainSelector): SatoshiScript?
{
    val constraintpush = OP.parse(bytes)
    val d = constraintpush.data
    if (d == null) return null
    val scr = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, d)
    val parsedScr = scr.parsed()
    for (inst in parsedScr)  // A constraint script must be push only
    {
        val pinst = OP.parse(inst)
        // not push only
        if (pinst.data == null && pinst.number == null) return null
    }
    return scr
}

/** Parse an input script into the 3 scripts it contains.
 * Note that the constraints have the option to either be in script form or pushes in the output, so this might not be sufficient
 * for evaluation.
 */
fun SatoshiScript.parseTemplateSpend(txo: NexaTxOutput?): SpecifiedScriptTemplate
{
    if (txo == null)
    {
        val p = parsed()
        val tmplpush = OP.parse(p[0])
        if (tmplpush.data == null) throw ScriptException("cannot parse: missing data push in template script location")
        val template = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, tmplpush.data!!)
        var drop = 1
        // without seeing the txo, we check if this appears to be a constraint args script push. If not, we assume that
        // the constraint args script does not exist.
        var constraint = probablyConstraintScript(p[1], chainSelector)
        if (constraint!=null) drop+=1
        else constraint = SatoshiScript(chainSelector)
        val satisfier = SatoshiScript(chainSelector, p.drop(drop).toMutableList())
        return SpecifiedScriptTemplate(satisfier, constraint, template)
    }
    else
    {
        val st = txo.script.parseTemplate(txo.amount)
        if (st == null) throw ScriptException("not a script template")
        if (st.wellKnownId == null)
        {
            val p = parsed()

            val tmplpush = OP.parse(p[0])
            if (tmplpush.data == null) throw ScriptException("cannot parse: missing data push in template script location")
            val template = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, tmplpush.data!!)
            var drop = 1
            val ah = st.argsHash
            val constraint = if (ah != null && ah.size != 0)  // if locking script has no constraint args hash, then it does not exist in unlocking
            {
                val constraintpush = OP.parse(p[1])
                if (!(constraintpush.inst contentEquals OP.PUSHFALSE.v) && constraintpush.data == null)
                    throw ScriptException("cannot parse: missing data push in constraint script location")
                val c = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, constraintpush.data ?: byteArrayOf())
                drop++
                c
            }
            else SatoshiScript(chainSelector)
            constraint.add(st.rest) // append the visible args to the constraint args script

            val satisfier = SatoshiScript(chainSelector, p.drop(drop).toMutableList())
            return SpecifiedScriptTemplate(satisfier, constraint, template)
        }
        else if (st.wellKnownId == P2PKT_ID.v[0].toPositiveLong())
        {
            val p = parsed()
            val styp = SatoshiScript.Type.SATOSCRIPT
            val template = SatoshiScript(chainSelector, styp, OP.FROMALTSTACK, OP.CHECKSIGVERIFY)

            var drop = 0
            val ah = st.argsHash
            val constraint = if (ah != null && ah.size != 0) // if locking script has no constraint args hash, then it does not exist in unlocking
            {
                val constraintpush = OP.parse(p[0])
                if (!(constraintpush.inst contentEquals OP.PUSHFALSE.v) && constraintpush.data == null)
                    throw ScriptException("cannot parse: missing data push in constraint script location")
                val c = SatoshiScript(chainSelector, SatoshiScript.Type.SATOSCRIPT, constraintpush.data ?: byteArrayOf())
                drop += 1
                c
            }
            else SatoshiScript(chainSelector)
            constraint.add(st.rest)  // append the visible args to the constraint args script

            val satisfier = SatoshiScript(chainSelector, p.drop(drop).toMutableList())
            return SpecifiedScriptTemplate(satisfier, constraint, template)
        }
        throw ScriptException("cannot parse")
    }
}

/** If you provide a parent and child transaction, this function will look at those transactions and return the DebuggerEnvironment
 * needed to verify the spend that connects them.
 *
 * If the child spends more than one output of the parent, this function will just set up for analysis of the first one
 * @param provide 2 transactions in a list.  You can actually provide many transactions and this function will set up for the first dependency.
 * @returns The data needed to start debugging, or null if there is no dependency in the passed transactions
 */
fun analyze2Tx(txes: List<NexaTransaction>): ScriptMachineEnvironment?
{
    // Figure out the outpoint to output connections
    val utxoMap = mutableMapOf<NexaTxOutpoint, Pair<Int,iTxOutput>>()
    for (t in txes)
    {
        val outpoints = t.outpoints
        var idx = 0
        for((point, output) in outpoints.zip(t.outputs))
        {
            utxoMap[point] = Pair(idx,output)
            idx++
        }
    }

    // ok look for the input that spends an output
    for (t in txes)
    {
        for (i in t.inputs)
        {
            val output = utxoMap[i.spendable.outpoint]
            if (output != null)
            {
                return ScriptMachineEnvironment(output.second as NexaTxOutput, output.first, i as NexaTxInput, t)
            }
        }
    }
    return null
}

/** The Script Machine binds to the virtual machine that processes Nexa scripts.
 *  This is the main class of this library.  You may create multiple script machine instances, and use member function
 *  calls to execute or step through isolated individual scripts or complete script template execution environments.
 */
open class ScriptMachine
{
    /** What chain this Script Machine works for.
     * Currently there is no deviation between Nexa mainet, testnet, and regtest so this parameter does not matter
     * */
    var chainSelector: ChainSelector = ChainSelector.NEXA
    /** The transaction (evaluation context for introspection opcodes) */
    var tx: iTransaction? = null
    /** The UTXOs being spent.  Transaction [tx] input n is assumed to be spending coin n */
    var coins: Array<iTxOutput>? = null
    /** Which input is this Script VM working on */
    var inputIdx = -1

    protected var smHandle: Long = 0

    /** The currently loaded template script */
    var template: SatoshiScript? = null
    /** The currently loaded constraint script (the holder's parameters that further constrain the template) */
    var constraint: SatoshiScript? = null
    /** The currently loades satisfier (unlocking) script */
    var satisfier: SatoshiScript? = null

    /** String name of the script I'm currently executing ("satisfier", "constraint", or "template") */
    var evaling: String? = null

    protected var whichScript = -1 // Which script am I currently on
    protected var scriptPos:Int? = 0
    /** True if execution is complete */
    var atEnd=false
    /** The prior script error, or "ok" if no error */
    var scriptErr: String? = null

    /** Returns the currently executing script (satisfier, constraint, or template) */
    val currentScript:SatoshiScript?
    get() = when(whichScript)
    {
        0 -> constraint
        1 -> satisfier
        2 -> template
        else -> null
    }

    /** Create a context-free script VM for evaluation of scripts that do not use transaction state.
     * Introspection and signature opcodes will not work because the transaction context is not provided.
     * This constructor can also be used alongside an initialize(...) function for 2-phase initialization.
     */
    constructor()
    {
        smHandle = org.nexa.libnexakotlin.ScriptMachine.createNoContext()
        LogIt.info("script machine handle " + smHandle.toString())
    }

    /** Wrap an existing underlying script machine
     */
    constructor(handle: Long)
    {
        smHandle = handle
        LogIt.info("script machine handle " + smHandle.toString())
    }


    /** Provide two scripts (as serialized hex) where one script's output is spent by the other script.
     * The Script VM will be initialized to evaluate the connected input/output.
     */
    constructor(txa: String, txb: String, chainSelector: ChainSelector = ChainSelector.NEXA) : this(txa.fromHex(), txb.fromHex(), chainSelector)

    /** Provide two scripts (as serialized binary) where one script's output is spent by the other script.
     * The Script VM will be initialized to evaluate the connected input/output.
     */
    constructor(txaBA: ByteArray, txbBA: ByteArray, chainSelector: ChainSelector = ChainSelector.NEXA)
        : this(
        NexaTransaction(chainSelector, BCHserialized(txaBA, SerializationType.NETWORK)),
        NexaTransaction(chainSelector, BCHserialized(txbBA, SerializationType.NETWORK))

    )

    /** Provide two scripts (as serialized hex) where one script's output is spent by the other script.
     * The Script VM will be initialized to evaluate the connected input/output.
     */
    constructor(txa: String, txb: String, _script: SatoshiScript, chainSelector: ChainSelector = ChainSelector.NEXA) : this(txa.fromHex(), txb.fromHex(), _script, chainSelector)

    /** Provide two scripts (as serialized binary) where one script's output is spent by the other script.
     * The Script VM will be initialized to evaluate the connected input/output.
     */
    constructor(txaBA: ByteArray, txbBA: ByteArray, _script: SatoshiScript, chainSelector: ChainSelector = ChainSelector.NEXA)
            : this(
        NexaTransaction(chainSelector, BCHserialized(txaBA, SerializationType.NETWORK)),
        NexaTransaction(chainSelector, BCHserialized(txbBA, SerializationType.NETWORK)),
        _script
                  )


    /** Create a script machine that executes input _inputIdx of transaction _tx.
     * This will only work if the input is spending a script template (and the script template does not access
     * prevouts via introspection), and does not push "public" constraint arguments since in that form all
     * the necessary scripts (satisfier, constraint, template) are located in the input script
    */
    constructor(_tx: NexaTransaction, _inputIdx: Int, utxo: NexaTxOutput?, advance: Boolean = true)
    {
        val result = _tx.inputs[_inputIdx].script.parseTemplateSpend(utxo)
        template = result.template
        constraint = result.constraint
        satisfier = result.satisfier
        inputIdx = _inputIdx
        tx = _tx
            // We don't have ANY inputs so initialize fakes with bogus values
        coins = Array(_tx.inputs.size, { i -> if ((i==_inputIdx)&&(utxo!=null)) utxo else NexaTxOutput(0, SatoshiScript(chainSelector)) })

        // Set the state to execute the first script
        scriptPos = null
        whichScript = -1
        initialize(advance)
    }

    /** Provide two scripts where one script's output is spent by the other script.
     * The Script VM will be initialized to evaluate the connected input/output.
     * @param _script  You can override the script template in the transactions with a custom script.  This is useful when debugging scripts.
     */
    constructor(txa: NexaTransaction, txb: NexaTransaction, _script: SatoshiScript? = null)
    {
        val poutmap = mutableMapOf<NexaTxOutpoint, iTxOutput>()
        for (out in txa.outpoints.zip(txa.outputs)) poutmap[out.first] = out.second
        for (out in txb.outpoints.zip(txb.outputs)) poutmap[out.first] = out.second

        // now look for an output that spends an input we know about
        // We are getting the first (TODO: allow it to be selected)
        for ((index, i) in txb.inputs.withIndex())
        {
            val _prevout = poutmap[i.spendable.outpoint]
            if (_prevout != null)
            {
                initScripts(i as NexaTxInput, _prevout as NexaTxOutput)
                initialize(index, txb, _prevout, _script)
                return
            }
        }
        for ((index, i) in txa.inputs.withIndex())
        {
            val _prevout = poutmap[i.spendable.outpoint]
            if (_prevout != null)
            {
                initScripts(i as NexaTxInput,_prevout as NexaTxOutput)
                initialize(index, txa, _prevout, _script)
                return
            }
        }

        // There was no relationship between the provided tx so we can't figure out which spend is interesting to the user
        // With a provided script, just assume that txb is the tx context and txa output 0 is the prevout
        // these assumptions will probably need to change based on use
        // and we should notify the user about these assumptions
        if (_script != null)
        {
            initialize(0, txb, txa.outputs[0] as NexaTxOutput, _script)
            return

        }
        throw ScriptMachineException("No spend", "These transaction are not related by a spend")
    }


    /** Provide an input and the output that spends it to initialize this VM's scripts.
     * Since a transaction is not provided, the expectation is that this script does not do introspection.
     * @param inp Nexa transaction input (spends [out])
     * @param out Nexa transaction output (the UTXO being spent)
     */
    fun initScripts(inp: NexaTxInput, out: NexaTxOutput)
    {
        val cs = out.chainSelector
        val st = SerializationType.NETWORK
        if (out.type == NexaTxOutput.Type.SATOSCRIPT)
        {
            template = null
            constraint = out.script
            satisfier = inp.script
        }
        else if (out.type == NexaTxOutput.Type.TEMPLATE)
        {
            val tmpl = out.script.parseTemplate(out.amount)
            if (tmpl != null)
            {
                val ins = inp.script.parsed()  // chunk it up
                if (tmpl.wellKnownId == null)
                {
                    template = SatoshiScript(cs, BCHserialized(ins[0], st))  // Template is 1st push
                    val c = SatoshiScript(cs, BCHserialized(ins[1], st))  // Constraint is 2nd push + rest of output
                    for (r in tmpl.rest) c.add(r)
                    constraint = c
                    ins[0] = byteArrayOf()  // Blow these away & the satisfier is the rest
                    ins[1] = byteArrayOf()
                    satisfier = SatoshiScript(cs, ins)
                }
                else
                {
                    if (tmpl.wellKnownId == 1L)
                    {
                        template = p2pkt
                        val c = SatoshiScript(cs, BCHserialized(ins[0], st))  // Constraint is 1st push + rest of output
                        for (r in tmpl.rest) c.add(r)
                        constraint = c
                        ins[0] = byteArrayOf()  // Blow these away & the satisfier is the rest
                        satisfier = SatoshiScript(cs, ins)
                    }

                }
            }
            else
            {

            }
        }
    }

    /** Initialize this script machine with a transaction and the prevout that you want to run.
     * We don't have all the prevouts so initialize what we don't have with bogus values.
     * The expectation is that the script doesn't use introspection to look at other prevouts (if so, don't use this function)
     * @param _inputIdx Which input of the transaction is being evaluated
     * @param _tx The transaction (for introspection context)
     * @param prevout The UTXO being consumed (and providing the scripts)
     * @param scriptOverride (optional) If non-null this script template is executed instead of what exists in the transaction and prevout
    */

    fun initialize(_inputIdx: Int, _tx: NexaTransaction, prevout: NexaTxOutput, scriptOverride: SatoshiScript? = null)
    {
        delete()

        inputIdx = _inputIdx
        tx = _tx
        chainSelector = _tx.chainSelector
        coins = Array(_tx.inputs.size, { if (it == inputIdx) prevout else NexaTxOutput(0, SatoshiScript(chainSelector)) })
        if (scriptOverride != null) template = scriptOverride
        val cb = BCHserialized(SerializationType.NETWORK) + coins!!.toMutableList()

        if (template == null)
        {
            smHandle = org.nexa.libnexakotlin.ScriptMachine.create(_tx.BCHserialize(SerializationType.NETWORK).toByteArray(), cb.toByteArray(), _inputIdx)
            whichScript = 0
            atEnd = false
            scriptPos = null
            LogIt.info("script machine handle " + smHandle.toString())
        }
        else
        {
            smHandle = org.nexa.libnexakotlin.ScriptMachine.createTemplateContext(_tx.BCHserialize(SerializationType.NETWORK).toByteArray(), cb.toByteArray(), satisfier?.toByteArray() ?: byteArrayOf(), constraint?.toByteArray() ?: byteArrayOf(), _inputIdx )
            whichScript = 2 // already executed the other 2, so set up for the top of the template script
            atEnd = false
            scriptPos = null
            LogIt.info("script machine handle " + smHandle.toString())
        }

    }

    /** Initialize the script machine with the data provided in member variables such as [tx], [inputIdx], and [coins]
     * If advance is true, execute the satisfier and constraint scripts, and move their results into the proper stack so we are ready for the template script
     * */
    fun initialize(advance:Boolean=true)
    {
        delete()

        val _tx = tx
        val _inputIdx = inputIdx
        val _coins = coins


        if (_tx != null && _coins != null)
        {
            val cb = BCHserialized(SerializationType.NETWORK) + _coins.toMutableList()
            if (advance && (template!=null))
            {
                smHandle = org.nexa.libnexakotlin.ScriptMachine.createTemplateContext(_tx.BCHserialize(SerializationType.NETWORK).toByteArray(), cb.toByteArray(), satisfier?.toByteArray() ?: byteArrayOf(), constraint?.toByteArray() ?: byteArrayOf(), _inputIdx )
                whichScript = 2 // already executed the other 2
                atEnd = false
                scriptPos = null
            }
            else
            {
                smHandle = org.nexa.libnexakotlin.ScriptMachine.create(_tx.BCHserialize(SerializationType.NETWORK).toByteArray(), cb.toByteArray(), _inputIdx)
                whichScript = 0  // line up for the first script
                atEnd = false
                scriptPos = null
            }
            LogIt.info("script machine handle " + smHandle.toString())
        }
        else
        {
            smHandle = org.nexa.libnexakotlin.ScriptMachine.createNoContext()
            whichScript = -1
            atEnd = true
            scriptPos = null
            LogIt.info("script machine (no context) handle " + smHandle.toString())
        }
    }

    /** Load the script machine stacks with the passed values.  Does not remove any existing items.
     * This occurs by creating two scripts and executing them.
     * */
    fun loadStacks(stack: List<Any>, alt: List<Any>)
    {
        swapStacks()  // In case there's stuff on the stacks already
        if (true)
        {
            val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
            for (i in alt)
            {
                when (i)
                {
                    is Int       -> bin.add(OP.push(i))
                    is Long      -> bin.add(OP.push(i))
                    is ByteArray -> bin.add(OP.push(i))
                }
            }
            val result = this.eval(bin)
            assert(result)
        }
        swapStacks()
        if (true)
        {
            val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
            for (i in stack)
            {
                when (i)
                {
                    is Int       -> bin.add(OP.push(i))
                    is Long      -> bin.add(OP.push(i))
                    is ByteArray -> bin.add(OP.push(i))
                }
            }
            val result = this.eval(bin)
            assert(result)
        }
    }

    /** Sets the resource use limits of the script machine.  Pass -1 to not change a limit (default)
     * @param smId Script machine id, provided by [create]
     * @param maxScriptSize  Maximum size of the script, in bytes
     * @param maxOps Maximum number of instructions to execute (ignoring 1 byte constants: OP_0 to 16, and multisig counts extra)
     * @param maxSignatureChecks Maximum number of actual signature check operations to be executed
     * @param maxStackUse Maximum allowable size of both stacks in bytes
     * @param maxStackItems Maximum number of items on both stacks
     * @param maxOpExec Maximum number of executed op_exec instructions
     * @param maxOpExecDepth Maximum recursion depth of op_exec calls
     */

    @Synchronized fun setLimits(maxScriptSize:Long=-1, maxOps:Long=-1, maxSignatureChecks:Long=-1, maxStackUse:Long=-1, maxStackItems:Long=-1, maxOpExec:Long=-1, maxOpExecDepth:Long=-1)
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        setResourceLimits(smHandle, maxScriptSize, maxOps, maxSignatureChecks, maxStackUse, maxStackItems, maxOpExec, maxOpExecDepth)
    }


    @Synchronized
    protected fun finalize()
    {
        if (smHandle != 0L)
        {
            org.nexa.libnexakotlin.ScriptMachine.delete(smHandle)
            smHandle = 0L
        }
    }

    /** Release the underlying Script Virtual Machine.
     * You should do this to clean up memory proactively, rather than relying on the garbage collector.
     * Once delete() has been called, you can call [initialize] to create a new instance of the virtual machine. */
    @Synchronized
    fun delete()
    {
        scriptErr = null
        scriptPos = null
        evaling = null
        atEnd=false

        if (smHandle != 0L)
        {
            org.nexa.libnexakotlin.ScriptMachine.delete(smHandle)
            smHandle = 0L
        }
    }

    /** Modify (overwrite) the current script starting at the provided location with the passed bytes
     * @return true if the modification worked, false if the provided position was out-of-bounds
     * @throw ScriptMachineException if the script machine is not initialized
     */
    fun modify(start: Int, bytes: ByteArray):Boolean
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        return org.nexa.libnexakotlin.ScriptMachine.modify(smHandle, start, bytes)
    }

    /** Modify (overwrite) the current script at the provided location with the passed opcode */
    fun modify(start: Int, opcode: OP):Boolean = modify(start, opcode.v)

    /** Resets (zeros) current resource use
     * @throw ScriptMachineException if the script machine is not initialized
     */
    fun resetResourceUse()
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        org.nexa.libnexakotlin.ScriptMachine.resetResourceUse(smHandle)
    }

    /** Executes the passed script opcodes
     *
     * @return True if the script executed till the end, False if it aborted.  This is DIFFERENT than the success/failure result of the script,
     * @throw ScriptMachineException if the script machine is not initialized
     */
    fun eval(vararg opcodes: OP):Boolean = eval(SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, *opcodes))

    /** Executes the passed script.
     *
     * @param run If run is true, the script executes until complete or a breakpoint is hit, if false it installs the script and is ready for stepping
     * @return True if the script executed till the end, False if it aborted.  This is DIFFERENT than the success/failure result of the script,
     * @throw ScriptMachineException if the script machine is not initialized.
     */
    fun eval(s: SatoshiScript, run:Boolean=true): Boolean
    {
        // If we are just inited, load this script into the first slot
        if (whichScript == -1) whichScript = 0
        // Load the correct variable with the script we are running
        when (whichScript)
        {
            0->constraint = s
            1->satisfier = s
            2->template = s
        }
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        scriptErr = null
        val ret = org.nexa.libnexakotlin.ScriptMachine.eval(smHandle, s.toByteArray(), run)
        if (run)
        {
            // Ran the whole script, so load some result state
            scriptPos = org.nexa.libnexakotlin.ScriptMachine.getPos(smHandle)
            scriptErr = org.nexa.libnexakotlin.ScriptMachine.getError(smHandle)
            atEnd = true
        }
        else  // Didn't run anything; just set up for it.
        {
            scriptPos = 0
            atEnd = false
        }
        return ret
    }

    /** Execute 1 instruction.
     * @return The success/failure of the executed instruction */
    fun step(): Boolean
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")

        // If we are stepping through a breakpoint we need to restore the code, step, and then put the breakpoint back
        val pos = getPos(smHandle)
        var breakpoint = false
        if (breakpointExists(pos))
        {
            clearBreakpoint(pos)  // restore code
            breakpoint = true
        }

        try
        {
            val ret = org.nexa.libnexakotlin.ScriptMachine.step(smHandle)
            scriptPos = org.nexa.libnexakotlin.ScriptMachine.getPos(smHandle)
            scriptErr = org.nexa.libnexakotlin.ScriptMachine.getError(smHandle)
            if (breakpoint) setBreakpoint(pos) // restore breakpoint
            return ret
        }
        catch(e: IllegalStateException)
        {
            if (breakpoint) setBreakpoint(pos) // restore breakpoint
            if (e.message == "completed")
            {
                scriptErr = "completed"
                scriptPos = null
                atEnd = true
                return false
            }
            else throw e
        }
    }

    val breakpoints:MutableMap<Pair<Int,Int>, ByteArray> = mutableMapOf()

    /** Set a breakpoint.
     * This places an illegal instruction in the specified location causing evaluation to stop when it is hit.
     * @throw ScriptMachineException if no script is currently set to execute
     * */
    fun setBreakpoint(offset: Int, script: Int? = null)
    {
        val whichS = script ?: whichScript  // If unspecified use current one
        if ((script == null)||(script == whichScript))  // current one -- actually install into the sm
        {
            val cs = currentScript
            if (cs == null) throw ScriptMachineException("no current script")
            val curByte = byteArrayOf(cs.toByteArray()[offset])
            modify(offset, byteArrayOf(-1))
            breakpoints[Pair(whichS,offset)] = curByte
        }
        else
        {
            throw ScriptMachineException("installing breakpoints in not-the-current-script is not yet implemented")
        }
    }

    /** Clear a breakpoint you previously set.
     * @return true if this breakpoint exists
     * @throw ScriptMachineException if no script is currently set to execute
     * */
    fun clearBreakpoint(offset: Int, script: Int? = null): Boolean
    {
        val whichS = script ?: whichScript  // If unspecified use current one
        if ((script == null)||(script == whichScript))  // current one -- actually install into the sm
        {
            val cs = currentScript
            if (cs == null) throw ScriptMachineException("no current script")
            val key = Pair(whichS,offset)
            val origBytes = breakpoints[key]
            if (origBytes == null) return false  // unknown breakpoint
            modify(offset, origBytes)
            breakpoints.remove(key)
        }
        else
        {
            throw ScriptMachineException("installing breakpoints in not-the-current-script is not yet implemented")
        }
        return true
    }

    /** Check if a breakpoint exists in this location.
     * @return true if a breakpoint exists in this location */
    fun breakpointExists(offset: Int, script: Int? = null):Boolean
    {
        val whichS = script ?: whichScript  // If unspecified use current one
        return breakpoints[Pair(whichS,offset)] != null
    }

    /** Continue execution.  This function will continue script execution until it completes, fails, or hits a breakpoint.
     * @return The success/failure of the last instruction executed. */
    fun cont(relativePos:Int=0): Boolean
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        if (relativePos != 0)
        {
            setPos(smHandle, getPos(smHandle) + relativePos)
        }
        else // Check to see if stopped in breakpoint.  If so have to restore code, back up, execute, replace breakpoint, continue
        {
            val bpos = getPos(smHandle)
            if ((scriptErr?.contains("Opcode missing") ?: false) && breakpointExists(bpos))
            {
                clearBreakpoint(bpos)  // restore code
                setPos(smHandle,bpos)  // back up
                step()  // execute restored code
                setBreakpoint(bpos) // restore breakpoint
            }
        }
        val ret = org.nexa.libnexakotlin.ScriptMachine.cont(smHandle)
        val bpos = getPos(smHandle)-1
        if ((getError(smHandle).contains("Opcode missing") ?: false) && breakpointExists(bpos))
        {
            setPos(smHandle,bpos)  // back up to the breakpoint
        }
        scriptPos = getPos(smHandle)
        scriptErr = getError(smHandle)
        return ret
    }

    /** Return the Script VM's Bignum modulo divisor */
    var bmd:String
        get()
        {
            return getBMD(smHandle)
        }
        set(s: String)
        {
            setBMD(s)
        }

    /** Return whether the VM is currently in an errored state or not */
    val status:String
        get()
        {
            var ret = scriptErr
            if (ret == null)
            {
                ret = getError(smHandle)
                scriptErr = ret
            }
            return ret
        }

    fun clearStatus()
    {
        scriptErr = null
        org.nexa.libnexakotlin.ScriptMachine.clearError(smHandle)
    }

    /** Return the current resources used by this script machine */
    fun getResources(): ScriptMachineResources
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        return(ScriptMachineResources(smHandle))
    }
    /** Update the passed object with the current resources used by this script machine */
    fun get(s: ScriptMachineResources)
    {
        if (smHandle == 0L) throw ScriptMachineException("uninitialized")
        s.get(smHandle)
    }

    /** Return the current program counter of the VM */
    var pos:Int
        get()
        {
            var p = scriptPos
            if (p == null)  // TODO I can't overwrite scriptPos if it has not even started a script
            {
                p = getPos(smHandle)
                scriptPos = p
            }
            return p
        }
        set(v: Int)
        {
            scriptPos = v
            setPos(smHandle,v)
        }

    fun getRegister(idx: Int): String
    {
        return org.nexa.libnexakotlin.ScriptMachine.getRegisterText(smHandle, idx)
    }

    fun setRegister(idx: Int, value: Long): String
    {
        return org.nexa.libnexakotlin.ScriptMachine.setRegisterText(smHandle, idx, "INTEGER(DEC)", value.toString())
    }

    fun setRegister(idx: Int, value: BigInteger): String
    {
        return org.nexa.libnexakotlin.ScriptMachine.setRegisterText(smHandle, idx, "BIGNUM(DEC)", value.toString())
    }

    fun setRegister(idx: Int, value: ByteArray): String
    {
        return org.nexa.libnexakotlin.ScriptMachine.setRegisterText(smHandle, idx, "BYTES", value.toHex())
    }

    fun setRegisterToBigNum(idx: Int, bignumMagSign: String): String
    {
        return org.nexa.libnexakotlin.ScriptMachine.setRegisterText(smHandle, idx, "BIGNUM", bignumMagSign)
    }

    /** Return the data in the main stack at the passed index
     * Since the stack data is complex, a string is returned of the following format:
     * {type} {length} {hex}h {decimal}>
     * For example: "BYTES 1 0ah 10"
     * */
    fun mainStackAt(idx: Int): String
    {
        val ret = getStackItemText(smHandle, 0, idx)
        return ret
    }

    /** Return the data in the alt stack at the passed index
     * Since the stack data is complex, a string is returned of the following format:
     * <TYPE> <length> <hex>h <decimal>
     * For example: "BYTES 1 0ah 10"
     * */

    fun altStackAt(idx: Int): String
    {
        val ret = getStackItemText(smHandle, 1, idx)
        return ret
    }

    /** Swap the data in the main stack and the alt stack */
    fun swapStacks()
    {
        org.nexa.libnexakotlin.ScriptMachine.swapStacks(smHandle)
    }


    /** Return value of [next], indicating that a special operation occurred in this step */
    enum class SpecialOperation { NONE, ALT_STACK_LOADED, ALL_DONE }


    /** Do the next operation in script processing.  This may be preparing the script machine for execution of the next script, and optionally running it.
     * Or it may be stepping forward 1 instruction.
     * This function is typically used when the script machine has been loaded with a full complement of scripts -- that is, satisfier, constraint, and template.
     * It is not needed if you are just [eval]-ing a script that you are providing as an argument.
     * @runNow If true, start running the script, otherwise pause just before the first instruction (@see [step]).
     * @return Triple of <what happened: which script was prepared to execute or step if we executed 1 instruction: string of constraint | satisfier | template | step, operation result: string of ok | error, what special operation was run (if any) >*/
    fun next(runNow:Boolean = true): Triple<String?,String, SpecialOperation>
    {
        if ((scriptPos == null) || (atEnd))  // Haven't started a script so figure out which script to execute and start that
        {
            var result: Boolean = true
            var resultString: String = ""
            var specOp = SpecialOperation.NONE
            scriptErr = null  // reset it
            evaling = null
            if (atEnd)  // Go to the beginning of the next script
            {
                whichScript++
                atEnd = false
                scriptPos = null
            }


            if (whichScript == 0)
            {
                val s = constraint
                if (s != null)
                {
                    result = eval(s, runNow)
                    evaling = "constraint"
                } else whichScript++ // missing so go on
            }

            if (whichScript == 1)
            {
                val s = satisfier
                if (s != null)
                {
                    swapStacks()
                    specOp = SpecialOperation.ALT_STACK_LOADED
                    result = eval(s, runNow)
                    evaling = "satisfier"
                }
                else whichScript++ // missing so go on
            }

            if (whichScript == 2)
            {
                val s = template
                if (s != null)
                {
                    result = eval(s, runNow)
                    evaling = "template"
                }
            }

            if (evaling == null)
            {
                evaling = "all scripts completed"
                specOp = SpecialOperation.ALL_DONE
            }
            else
            {
                if (resultString == "") resultString = getError(smHandle)
            }

            return Triple(evaling, resultString, specOp)
        }
        else
        {
            val r = if (runNow) cont() else step()
            return Triple("step", if (r) "ok" else "error", SpecialOperation.NONE)
        }
    }

    /** Get a succinct description of the machine's current state */
    fun getState(): MachineState
    {
        var b = mutableListOf<String>()
        var stk:String = ""
        var i = 0
        do
        {
            stk = mainStackAt(i)
            if (stk != "")
            {
                b.add(stk)
                i++
            }
        }
        while (stk != "")
        b.reverse()

        i = 0
        var a = mutableListOf<String>()
        do
        {
            stk = altStackAt(i)
            if (stk != "")
            {
                a.add(stk)
                i++
            }
        }
        while (stk != "")
        a.reverse()
        // Don't override scriptPos if its null, because that sets up for the next script
        val p = if (scriptPos == null) null else pos
        return MachineState(evaling, p, status, bmd, b, a)
    }
    fun setupCurrentScript()
    {
        when (evaling)
        {
            "satisfier" -> eval(satisfier!!, false)
            "constraint" -> eval(constraint!!, false)
            "template" -> eval(template!!, false)
            else -> {
                // If we are just running any old script (not the full 3 party interaction), then we put it in the template
                if (template != null) eval(template!!, false)
            }
        }
    }

    fun getBinaryStack(whichStack: ScriptMachineStack):List<Any>
    {
        var i = 0
        var ret = mutableListOf<Any>()
        do {
            val stk = if (whichStack==ScriptMachineStack.MAINSTACK) mainStackAt(i) else altStackAt(i)
            if (stk != "")
            {
                val split = stk.split(" ")
                when (split[0])
                {
                    "BYTES" -> {
                        val hex = split[2].dropLast(1)  // ends in an h for hex
                        ret.add(hex.fromHex())
                    }
                    "BIGNUM" -> {
                        val hex = split[2].dropLast(1)
                        ret.add(BigInteger.parseString(hex,16))
                    }
                }
                i++
            }
        } while (stk != "")
        ret.reverse()
        return ret
    }

    /** Sets the BMD of the machine to the passed BMD */
    fun setBMD(targetBMD: String)
    {
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
        val t = BigInteger.parseString(targetBMD, 16).toByteArray().reversed().toMutableList()
        t.add(0.toByte()) //As BMD is always positive
        bin.add(OP.push(t.toByteArray()))
        bin.add(OP.SETBMD)
        this.eval(bin)
    }

    fun clone(): ScriptMachine
    {
        val cloneHandle = org.nexa.libnexakotlin.ScriptMachine.clone(smHandle)
        val clone = ScriptMachine(cloneHandle)
        clone.template = this.template?.copy()
        clone.satisfier = this.satisfier?.copy()
        clone.constraint = this.constraint?.copy()
        clone.evaling = this.evaling
        clone.whichScript = this.whichScript
        clone.atEnd = this.atEnd
        clone.chainSelector = this.chainSelector
        clone.coins = this.coins
        clone.tx = this.tx
        clone.inputIdx = this.inputIdx
        clone.breakpoints.putAll(this.breakpoints)
        return clone
    }

    /** Returns a copy of the script machine */
    fun copy(): ScriptMachine
    {
        val clone = ScriptMachine()
        clone.loadStacks(getBinaryStack(ScriptMachineStack.MAINSTACK),getBinaryStack(ScriptMachineStack.ALTSTACK))
        clone.setupCurrentScript()
        clone.setBMD(this.bmd)
        clone.template = this.template?.copy()
        clone.satisfier = this.satisfier?.copy()
        clone.constraint = this.constraint?.copy()
        clone.evaling = this.evaling
        clone.whichScript = this.whichScript
        clone.atEnd = this.atEnd
        clone.chainSelector = this.chainSelector
        clone.coins = this.coins
        clone.tx = this.tx
        clone.inputIdx = this.inputIdx
        clone.setupCurrentScript()
        clone.pos = this.pos
        clone.scriptErr = this.scriptErr
        clone.breakpoints.putAll(this.breakpoints)
        if (scriptErr?.contains("completed") ?: false) clone.step()
        return clone
    }

    /** Overwrites the current stack with the passed stack
     * Maintains current machine state other than stack
     * */
    fun replaceStacks(stack: List<Any>, alt: List<Any>)
    {
        val copyPos = pos
        val copyTemplate = template
        val copySatisfier = satisfier
        val copyConstraint = constraint
        val copywWichScript = whichScript
        val copyEvaling = evaling
        val copyAtEnd = atEnd
        val copyScriptErr = scriptErr
        initialize(false)       // Clear the current stack
        loadStacks(stack, alt)          // Load the stack
        setupCurrentScript()            // Restore machine position
        pos = copyPos
        template = copyTemplate
        satisfier = copySatisfier
        constraint = copyConstraint
        whichScript = copywWichScript
        evaling = copyEvaling
        atEnd = copyAtEnd
        scriptErr = copyScriptErr
    }
}