package org.nexa.scriptmachine

import org.nexa.libnexakotlin.initializeLibNexa

/** Instantiate the library.
 * This class loads the Nexa Script Virtual Machine shared library (libnexa.so) and binds to it.
 * It must be instantiated before
 */
@OptIn(ExperimentalUnsignedTypes::class)
fun Initialize()
{
    try
    {
        initializeLibNexa()
    }
    catch (e: UnsatisfiedLinkError)
    {
        System.out.println("Error ${e.toString()}")
        System.out.println("Likely libnexa.so was NOT configured with the --enable-javacashlib flag enabled.")
        throw e
    }
}