package org.nexa.libnexakotlin
// This MUST be a different package so the function names align with those in libnexa.so

/** Track how many resources a script machine has used */
// Implementation note:  Do NOT change these field names since they need to match the names in libnexa.so JNI
data class ScriptMachineResources(
    /** How many signatures operations were executed */
    var sigsChecked:Long = 0,
    /** Number of instructions executed, excluding pushes of constants 0-16 (that is, OP_0 to OP_16) */
    var instructionsExecuted: Long = 0,
    /** Maximum number of bytes used in both stacks (sum of both stack's "high water marks") in bytes */
    var maxStackBytes: Long = 0,
    /** Maximum number of items in both stacks (sum of both stack's "high water marks") */
    var maxStackItems: Long = 0,
    /** Number of op_execs executed */
    var opExecsExecuted: Long = 0,
    /** Longest chain of recursive op_exec */
    var opExecRecursionDepth: Long = 0
    )
{
    external fun get(smid: Long)
    constructor(scriptMachineHandle: Long):this()
    {
        get(scriptMachineHandle)
    }
}

class ScriptMachine
{
    companion object
    {
        /** create a script machine instance */
        @JvmStatic
        external fun create(tx: ByteArray, outpoints: ByteArray, inputIdx: Int, flags: Int = -1): Long

        /** create no context script machine (tx introspection and sigchecks will fail) */
        @JvmStatic
        external fun createNoContext(flags: Int = -1): Long

        /** Create a script machine context with the satisfier and constraint scripts already loaded into their stacks */
        @JvmStatic
        external fun createTemplateContext(tx: ByteArray, outpoints: ByteArray, satisfier: ByteArray, constraint: ByteArray, inputIdx: Int, flags: Int = -1): Long

        /** delete the script machine */
        @JvmStatic
        external fun delete(smId: Long): Boolean

        /** run a script within the current context of the script machine.  If run is true, script is fully evaluated.  If false, script is loaded and ready for stepping */
        @JvmStatic
        external fun eval(smId: Long, script: ByteArray, run: Boolean): Boolean

        /** Get what's on the stack, pass 0 for mainstack, 1 for alt.
         * index=0 is the top of the stack, returns "" if you are asking for a non-existent index.
         * @Returns "FALSE" for a stack item that has no size (the result of OP_FALSE executed) */
        @JvmStatic
        external fun getStackItemText(smId: Long, whichStack: Int, index: Int): String

        /** Gets the contents of the passed register by 0-based index.
         * @Returns a string in the same format as getStackItemText */
        @JvmStatic
        external fun getRegisterText(smId: Long, index: Int): String

        /** Sets the contents of the passed register by 0-based index.
         * @param smId Script machine id, provided by [create]
         * @param itemType "BYTES","BIGNUM","BIGNUM(DEC)","INTEGER(DEC)".  The last two are convenience setters that allow you to provide the value in decimal.
         * @param itemValue If type is bytes provide Hex bytes, if bignum provide hex bytes in magnitude-sign format. If type is the last two, provide a decimal number.
         * @Returns a string of the resulting register's value in the same format as getStackItemText */
        @JvmStatic
        external fun setRegisterText(smId: Long, index: Int, itemType: String, itemValue: String): String

        /** Sets the resource use limits of the script machine.  Pass -1 to not change a limit
         * @param smId Script machine id, provided by [create]
         * @param maxScriptSize  Maximum size of the script, in bytes
         * @param maxOps Maximum number of instructions to execute (ignoring 1 byte constants: OP_0 to 16, and multisig counts extra)
         * @param maxSignatureChecks Maximum number of actual signature check operations to be executed
         * @param maxStackUse Maximum allowable size of both stacks in bytes
         * @param maxStackItems Maximum number of items on both stacks
         * @param maxOpExec Maximum number of executed op_exec instructions
         * @param maxOpExecDepth Maximum recursion depth of op_exec calls
         */
        @JvmStatic
        external fun setResourceLimits(smid:Long, maxScriptSize:Long, maxOps:Long, maxSignatureChecks:Long, maxStackUse:Long, maxStackItems:Long, maxOpExec:Long, maxOpExecDepth:Long)

        /** resets (clears) current resource use statistics.
         * @param smId Script machine id, provided by [create]
         */
        @JvmStatic
        external fun resetResourceUse(smid:Long)

        /** Get error string */
        @JvmStatic
        external fun getError(smId: Long): String

        /** Removes the current error condition */
        @JvmStatic
        external fun clearError(smId: Long)

        /* Swap main and alt stacks (useful for script template execution) */
        @JvmStatic
        external fun swapStacks(smId: Long)

        /* execute one instruction */
        @JvmStatic
        external fun step(smId: Long): Boolean

        /* continue until finished, or error */
        @JvmStatic
        external fun cont(smId: Long): Boolean

        /* return current position in script (in bytes) */
        @JvmStatic
        external fun getPos(smId: Long): Int

        /* sets the current position in the script (in bytes from the beginning) */
        @JvmStatic
        external fun setPos(smId: Long, offset: Int): Int

        /* return current Bignum modulo divisor as a hex string */
        @JvmStatic
        external fun getBMD(smId: Long): String

        /* sets the Bignum modulo divisor, provide a hex string */
        @JvmStatic
        external fun setBMD(smId: Long, bmdHex:String): String

        /* Create a copy (clone) of a script machine, returning the id of the clone.  You must [delete] the returned id! */
        @JvmStatic
        external fun clone(smId: Long): Long

        /* modify the script being executed */
        @JvmStatic
        external fun modify(smId: Long, offset: Int, data: ByteArray): Boolean
    }
}