# Module NexaScriptMachine

A library to access the Nexa script processing virtual machine from Kotlin.  This library allows you to execute and debug Nexa scripts using the actual script machine running within the Nexa full node.


## Add to your Gradle Project

[Find the latest library version here.](https://gitlab.com/api/v4/projects/46299034/packages)

In your build.grade.kts:

```gradle
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven") } // NexaScriptMachine
}

dependencies {
    implementation("Nexa","NexaScriptMachine","1.0.1")  // Update this version with the latest
}
```


**Note that libnexa.so (from the [https://gitlab.com/nexa/nexa](Nexa full node)) MUST be in your path or working directory!**  This file is provided as part of this library, but may need updating.

# Package Nexa.ScriptMachine 

*This is the main package to access the Nexa Script Virtual Machine*

*Simple Example*

``` kotlin
ScriptMachine.Initialize()  // Call ScriptMachine.Initialize() ONCE before using this library!

val sm = ScriptMachine()
val result = sm.eval(OP.push(1), OP.push(2), OP.ADD)
println(sm.mainStackAt(0))
```
output: `BYTES 1 03h 3`

*Interesting Example*

```kotlin
ScriptMachine.Initialize()  // Call ScriptMachine.Initialize() ONCE before using this library!
val sm = ScriptMachine(
    // these are real Nexa mainnet transactions.  One spends the other
    "000200cb159a8e52e2c74ad429c2de88f17ef319f86f9590a8fa1f098db38f313a2b6264222103b64731809e5f319da2e24bde8fd8e3932a8eff95e52f8c27fb404e28f1ccbd6f40f2243c823ecd9b47dd9ff898a01549b4a87bab9641596d97bb550b7cde6eaf68f15ebe4939579e28edc7a5065be2229be273a8e4e5f79ceb08cb855c1339c733feffffff8f32e732000000000020f555e60ef26d64afeac8d11b21ce257168e4d3fbb7b50c6e7a74d024db0f0b642221033e3785bf307e019a934953df869812d92529d731ad0dd680ce7f827d707725d4409167b36a8f3a5d1af679bc02133a800065159381884fcc618ef555cfd195e1e5a7f68bb68521c8a7e1333ebd5cdc046fd6e9f3a16fe1e3c63aa6c2e5f9f44865feffffff27800100000000000101d2aee83200000000170051149b95d76faca0caa4b0c70648a3f6aa279d887254c84c0400",
    "000400c87f900101d8ea4b80c015166e86e6edee7a46e7040cab812618066c41cff73b6422210388d574eef524c90d17d82c3616500265463d31fbc030699da33dd7f2dc280279400e520d58d96546333d228fc0e2acfce1844c3c2c87ce8e0c010492a8157bfcdbfe064d8571aa6a2e6b055b87ae96bb664add7e41508c17cf28aac08f20761123feffffff1ad0ff02000000000019d53ff57314cb2ee8d180050d4e7e055b6f6b8aa4a65871c9486960b185944f64222103efd9aa05dafd563fda6ecdb8dba647b95233f6f7d23abec07525a8c1881491a740c69c1b3d07bfe691cd1cafaae0424649e6313c4c3557765b05ddb0a783d117a2ead5ef7a9b7510bc65fb66387ae7a67754551cbd0a9b46f9b090aaf928939bc7feffffffa9db37000000000000c31e4fee1bfaa89f74a912ab3e2fb8cc3c55b8bf806fda59a2ad3c4bce9f735064222103b1f8d7d1f23e09edfa2fb8ab6f85f9511b7b946863fcddb507243d05052b8b05408389dbf9e303c47e5f961df1211afe934ec5393e3ac487de0cfc496eeedf5e372b3ed943977e462c528648c2a963fc0fac2aa327a0ef0c3e278e16592bae2a0dfeffffff850a0d000000000000fb3feaed598c907f6e905eb8e77326f3177a8ff7734608e0e5d6ace23bcc27a1642221026c2a2a3283ef5f308770c4e17f491428545c1e9cce714b2e1379ba28f093200940630f0ba44c4f51cde446ce6531da20a339aa3f7745f9acf936bbc51f57a36ccd7b3530c1ff5e6896d3d67742b0af22694bc6ebc0010388ecd666a4b2760a4da8feffffff0008af2f00000000020128890c000000000017005114f7ebf308b8396180f0cc63fc0b8ad686f24a1e8d018f32e7320000000017005114e919c8b37f76c08bdc1ec0ef38384e792c71aa6489470400"
                              )
    var ok = true
    println("Template is: " + sm.template!!.toAsm(" "))
    println("Constraint Arg is: " + sm.constraint!!.toAsm(" "))
    println("Satisfier Arg is: " + sm.satisfier!!.toAsm(" "))
    var instructionCount = 0
    sm.goScript(false)  // true means run, false means prepare to step
    do {
        ok = sm.step()
        if (ok) instructionCount++
    } while(ok)
    println("Result is: ${sm.scriptErr} in $instructionCount instructions")
    sm.delete()
```



