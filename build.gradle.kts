import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.File
import java.io.FileInputStream
import java.net.URL
import java.util.*

plugins {
    kotlin("jvm") version "2.1.20-Beta2"
    kotlin("plugin.serialization") version "2.1.10"
    id("maven-publish")
    id("org.jetbrains.dokka") version "2.0.0" // https://github.com/Kotlin/dokka/tags
}

group = "org.nexa"
version = "1.2.2"
val ktor_version = "3.0.3"
val kser_version = "1.6.3"
val dokka_version = "2.0.0" // https://github.com/Kotlin/dokka/tags
val bigNumVersion = "0.3.9" // https://github.com/ionspin/kotlin-multiplatform-bignum

val deployTokenKey: String by project

val prop = Properties().apply {
    load(FileInputStream(File(rootProject.rootDir, "local.properties")))
}

repositories {
    mavenCentral()
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
    maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpthreads
    mavenLocal()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kser_version")
    implementation("com.ionspin.kotlin:bignum:$bigNumVersion")
    //implementation("io.ktor:ktor-client-core:$ktor_version")
    //implementation("io.ktor:ktor-client-cio:$ktor_version")
    //implementation("io.ktor:ktor-serialization-kotlinx-json:$ktor_version")

    testImplementation("org.jetbrains.kotlin:kotlin-test:2.0.21")

    //dokkaHtmlPlugin("org.jetbrains.dokka:kotlin-as-java-plugin",dokka_version)

    // Nexa
    implementation("org.nexa:mpthreads:0.4.0") // https://gitlab.com/nexa/mpthreads/-/packages
    implementation("org.nexa:libnexakotlin:0.4.10") // https://gitlab.com/nexa/libnexakotlin/-/packages
}

buildscript {
    dependencies {
        //classpath("org.jetbrains.dokka:dokka-base", dokka_version)
        classpath("org.jetbrains.dokka:dokka-base:2.0.0")
    }
}


tasks.named<Test>("test") {
    useJUnitPlatform()
    outputs.upToDateWhen { false }  // always rerun
    testLogging {
        events("passed", "skipped", "failed")
        showStandardStreams = true
    }
}

tasks.withType<KotlinCompile> {
    compilerOptions {
        jvmTarget.set(JvmTarget.JVM_17)
    }
}

tasks.dokkaHtml {
    outputDirectory.set(rootDir.resolve("public"))
}

tasks.withType<DokkaTask>().configureEach {

    pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
        customAssets = listOf(file("nexa-64x64.png"),file("logo-icon.svg"))
        customStyleSheets = listOf(file("logo-styles.css"))
        footerMessage = "(c) 2024 Bitcoin Unlimited"
    }

    dokkaSourceSets {
        named("main") {
            //moduleName.set("Nexa Script Machine")
            displayName.set("Nexa Script Machine Library")
            includes.from("Module.md")
            sourceLink {
                localDirectory.set(file("src/main/kotlin"))
                remoteUrl.set(URL("https://gitlab.com/nexa/nexascriptmachinekotlin/-/tree/main/"))
                remoteLineSuffix.set("#L")
            }
        }
    }
    dokkaSourceSets.configureEach {

            perPackageOption {
                matchingRegex.set("bitcoinunlimited.*")
                suppress.set(true)
            }
        }

}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
        }

    }
    repositories {
        maven {
            // Project ID number is shown just below the project name in the project's home screen
            url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = prop.getProperty("DeployTokenValue")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

// Grab my libraries from the jars directory (for local development)
fun DependencyHandlerScope.localOrReleased(localFile: String, dependencyNotation: Any, forceLocal:Boolean? = null)
{
    val fl = if (forceLocal==null) project.property("localJars").toString().trim().toBooleanStrict()
    else forceLocal

    val hasLocal = if (fl)
    {
        val nexaJvm = File(rootProject.rootDir,localFile)
        if (nexaJvm.exists())
        {
            println("Using local library: $localFile")
            implementation(files(nexaJvm))
            true
        }
        else
        {
            println("WARNING: Even though LOCAL_JARS is true $localFile pulled from its released version because it" +
                    " does not exist at ${nexaJvm.absolutePath}.")
            false
        }
    } else false

    if (!hasLocal)
    {
        println("Using release library for: $localFile")
        implementation(dependencyNotation)
    }
}